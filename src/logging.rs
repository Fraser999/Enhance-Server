//! Helper function to allow initialisation and formatting of `env_logger`.

use env_logger::{fmt::Formatter, Builder};
use log::Record;
use std::io::Write;

/// Safe to call multiple time from multiple threads for cfg(test) only.  Otherwise, more than one
/// call to this function will raise a panic.
pub(crate) fn init() {
    let do_format = move |formatter: &mut Formatter, record: &Record<'_>| {
        let now = formatter.timestamp();
        writeln!(
            formatter,
            "{} {} [{}:{}:{}] {}",
            formatter.default_styled_level(record.level()),
            now,
            record
                .module_path()
                .unwrap_or_default()
                .splitn(2, "::")
                .next()
                .unwrap_or_default(),
            record.file().unwrap_or_default(),
            record.line().unwrap_or_default(),
            record.args()
        )
    };

    let mut builder = Builder::from_default_env();
    if cfg!(test) {
        let _ = builder.format(do_format).is_test(true).try_init();
    } else {
        builder.format(do_format).init();
    }
}
