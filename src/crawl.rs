//! Provides functionality required for a crawl of a single domain.

use crate::{domain::Domain, server::DOMAINS, HttpsClient};
use futures::{
    future::{self, Either, Loop},
    Future, Stream,
};
use hyper::{header, Body, Method, Request, Response};
use log::{info, trace, warn};
use select::{document::Document, predicate::Name};
use std::{
    collections::{hash_map::Entry, HashSet},
    fmt::{self, Debug, Formatter},
};
use unwrap::unwrap;
use url::{ParseError, Url};

/// A rough-and-ready upper limit on the size of a response to try and handle.
const MAX_CONTENT_LENGTH: usize = 1_000_000;
/// The only content-type to try and parse.
const REQUIRED_CONTENT_TYPE: &str = "text/html";

/// This is used to hold the state of a single ongoing crawl operation.
pub(crate) struct State {
    domain: Domain,
    client: HttpsClient,
    queried_urls: HashSet<String>,
    current_url: String,
    pending_urls: Vec<String>,
}

impl State {
    /// Returns a new `State` object in preparation for a new crawl.
    ///
    /// Returns an error if `domain_str` doesn't yield a valid domain, or if we're already crawling
    /// this domain.
    ///
    /// If the domain has already been crawled, this call to `new()` will clear the old data from
    /// the global results (replacing the entry with `None`) in preparation for the crawl.
    pub fn new(domain_str: &str, client: HttpsClient) -> Result<Self, String> {
        let domain = Domain::new(domain_str)?;

        let mut domains = unwrap!(DOMAINS.write());
        match domains.entry(domain.clone()) {
            Entry::Occupied(mut occupied) => {
                if occupied.get().is_some() {
                    *occupied.get_mut() = None;
                } else {
                    return Err(format!("Already crawling domain {}", domain_str));
                }
            }
            Entry::Vacant(vacant) => {
                let _ = vacant.insert(None);
            }
        }

        let pending_urls = vec![domain.scheme_and_domain().to_string()];
        Ok(Self {
            domain,
            client,
            queried_urls: HashSet::new(),
            current_url: String::new(),
            pending_urls,
        })
    }

    /// Handles a received page from the domain by storing any new URLs for this domain in the
    /// pending list.
    fn parse_page(&mut self, page_str: &str) {
        if page_str.is_empty() {
            return;
        }
        trace!("Parsing page {}", self.current_url);
        let page = Document::from(page_str);
        let current_query_url = unwrap!(Url::parse(&self.current_url));
        let urls = page
            .find(Name("a"))  // find all <a> tags
            .filter_map(|node| {
                node.attr("href")  // filter out links for other domains and strip anchors & queries
                    .and_then(|link| url_for_this_domain(link, &current_query_url))
            })
            .filter_map(|url| {  // filter URLs already indexed
                if self.queried_urls.contains(url.as_str()) {
                    None
                } else {
                    Some(url.as_str().to_string())
                }
            })
            .collect::<Vec<_>>();
        self.pending_urls.extend(urls);
        self.pending_urls.sort_unstable();
        self.pending_urls.dedup();
    }
}

impl Debug for State {
    fn fmt(&self, formatter: &mut Formatter<'_>) -> fmt::Result {
        write!(
            formatter,
            "State for {:?}\n  Queried: {:#?}\n  Pending: {:#?}\n\n",
            self.domain, self.queried_urls, self.pending_urls,
        )
    }
}

/// Starts a new crawl for the domain indicated in `state`.
///
/// Iteratively sends requests for each unique URL it finds for the given domain, then scrapes any
/// new URLs from the returned pages.  Once there are no new URLs returned, the results are stored
/// in the global `server::DOMAINS` map, and the crawl is finished.
pub(crate) fn crawl(state: State) -> Box<dyn Future<Item = (), Error = ()> + Send> {
    info!("Started crawling {}.", state.domain);
    Box::new(
        future::loop_fn(state, |state| {
            start_query(state).and_then(|(page, mut state)| {
                state.parse_page(&page);
                if state.pending_urls.is_empty() {
                    Ok(Loop::Break(state))
                } else {
                    Ok(Loop::Continue(state))
                }
            })
        })
        .and_then(|state| {
            store_results(state);
            future::ok(())
        }),
    )
}

/// Starts a single new query for a URL which hasn't been indexed yet.
fn start_query(mut state: State) -> Box<dyn Future<Item = (String, State), Error = ()> + Send> {
    let next_url = unwrap!(state.pending_urls.pop());
    trace!("Querying {}", next_url);

    let request = unwrap!(Request::builder()
        .method(Method::GET)
        .uri(&next_url)
        .body(Body::default()));
    assert!(state.queried_urls.insert(next_url.clone()));
    state.current_url = next_url;

    let f = state
        .client
        .request(request)
        .then(|response_result| match response_result {
            Ok(response) => {
                trace!(
                    "Got response from {}: {}",
                    state.current_url,
                    response.status(),
                );
                if !is_valid(&response, &state.current_url) {
                    return Either::A(Either::A(future::ok((String::new(), state))));
                };
                Either::A(Either::B(response.into_body().concat2().map(|full_body| {
                    let doc = String::from_utf8_lossy(full_body.as_ref()).to_string();
                    (doc, state)
                })))
            }
            Err(error) => {
                warn!("Got {:?} while querying {}", error, state.current_url);
                Either::B(future::ok((String::new(), state)))
            }
        })
        .map_err(|_| ());
    Box::new(f)
}

fn is_valid(response: &Response<Body>, current_url: &str) -> bool {
    is_text_html(&response)
        && match content_length(&response) {
            Some(length) if length < MAX_CONTENT_LENGTH => true,
            Some(length) => {
                warn!(
                    "Response from {} too long {}, so ignoring it.",
                    current_url, length
                );
                false
            }
            None => {
                warn!(
                    "Header from {} has no content-length, so ignoring it.",
                    current_url
                );
                false
            }
        }
}

fn content_length(response: &Response<Body>) -> Option<usize> {
    response
        .headers()
        .get(header::CONTENT_LENGTH)
        .and_then(|value| value.to_str().ok())
        .and_then(|value_str| value_str.parse().ok())
}

fn is_text_html(response: &Response<Body>) -> bool {
    response
        .headers()
        .get(header::CONTENT_TYPE)
        .and_then(|value| value.to_str().ok())
        .unwrap_or_default()
        .starts_with(REQUIRED_CONTENT_TYPE)
}

/// Returns `Some` if:
///   * `url_str` has the same domain as our current crawl, or
///   * if `url_str` is a relative link, joins it to `current_query_url` and returns this if it
///     still has the same domain as our current crawl.
fn url_for_this_domain(url_str: &str, current_query_url: &Url) -> Option<Url> {
    match Url::parse(url_str) {
        Ok(url) => {
            if url.domain() == current_query_url.domain() {
                Some(url)
            } else {
                None
            }
        }
        Err(ParseError::RelativeUrlWithoutBase) => current_query_url
            .join(url_str)
            .ok()
            .filter(|url| url.domain() == current_query_url.domain()),
        Err(_) => None,
    }
    .map(|mut url| {
        // Strip anchors, queries and a trailing '/' unless that's the complete path.
        url.set_fragment(None);
        url.set_query(None);
        let _ = url.path_segments_mut().map(|mut segments| {
            let _ = segments.pop_if_empty();
        });
        url
    })
}

/// Having finished crawling the specified domain, this stores the results in the global map.
fn store_results(mut state: State) {
    info!("Finished crawling {}.", state.domain);
    let urls = state.queried_urls.drain().collect();
    let mut domains = unwrap!(DOMAINS.write());
    let previous_value = unwrap!(domains.insert(state.domain, Some(urls)));
    // Check that the domain was held in the map with a `None` value to indicate crawling ongoing.
    assert!(previous_value.is_none());
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::logging;
    use hyper::{Body, Client};
    use hyper_tls::HttpsConnector;
    use std::{env, fs, path::Path};

    #[test]
    fn urls_for_this_domain() {
        logging::init();

        let url = unwrap!(Url::parse("http://a.com/1/2/3.html?page=2"));
        let mut test_str = "about";
        let mut expected = unwrap!(Url::parse("http://a.com/1/2/about"));
        assert_eq!(expected, unwrap!(url_for_this_domain(test_str, &url)));

        test_str = "/about";
        expected = unwrap!(Url::parse("http://a.com/about"));
        assert_eq!(expected, unwrap!(url_for_this_domain(test_str, &url)));

        test_str = "../about";
        expected = unwrap!(Url::parse("http://a.com/1/about"));
        assert_eq!(expected, unwrap!(url_for_this_domain(test_str, &url)));

        test_str = "4/about";
        expected = unwrap!(Url::parse("http://a.com/1/2/4/about"));
        assert_eq!(expected, unwrap!(url_for_this_domain(test_str, &url)));

        test_str = "https://a.com/about";
        expected = unwrap!(Url::parse("https://a.com/about"));
        assert_eq!(expected, unwrap!(url_for_this_domain(test_str, &url)));

        test_str = "?query";
        expected = unwrap!(Url::parse("http://a.com/1/2/3.html"));
        assert_eq!(expected, unwrap!(url_for_this_domain(test_str, &url)));

        test_str = "about?query";
        expected = unwrap!(Url::parse("http://a.com/1/2/about"));
        assert_eq!(expected, unwrap!(url_for_this_domain(test_str, &url)));

        test_str = "#anchor";
        expected = unwrap!(Url::parse("http://a.com/1/2/3.html"));
        assert_eq!(expected, unwrap!(url_for_this_domain(test_str, &url)));

        test_str = "about#anchor";
        expected = unwrap!(Url::parse("http://a.com/1/2/about"));
        assert_eq!(expected, unwrap!(url_for_this_domain(test_str, &url)));

        assert!(url_for_this_domain("http://b.net/1", &url).is_none());
    }

    fn urls_from_test_files(
        filename: &str,
        current_query_url: &str,
        mut expected_urls: Vec<String>,
    ) {
        logging::init();

        let resources_dir =
            Path::new(&unwrap!(env::var("CARGO_MANIFEST_DIR"))).join("test_resources");

        let page = unwrap!(fs::read_to_string(&resources_dir.join(filename)));

        let domain = unwrap!(Domain::new(current_query_url));
        let mut queried_urls = HashSet::new();
        let _ = queried_urls.insert(current_query_url.to_string());
        let https = unwrap!(HttpsConnector::new(4));
        let client = Client::builder().build::<_, Body>(https);
        let mut state = State {
            domain,
            client,
            queried_urls,
            current_url: current_query_url.to_string(),
            pending_urls: vec![],
        };

        state.parse_page(&page);
        expected_urls.sort_unstable();
        assert_eq!(expected_urls, state.pending_urls);
    }

    #[test]
    fn parse_duckduckgo() {
        urls_from_test_files(
            "duckduckgo.html",
            "http://duckduckgo.com",
            vec!["http://duckduckgo.com/about".to_string()],
        );
    }

    #[test]
    fn parse_rust_collections() {
        urls_from_test_files(
            "rust_collections.html",
            "https://doc.rust-lang.org/std/collections/btree_map/enum.Entry.html",
            vec![
                "https://doc.rust-lang.org/settings.html".to_string(),
                "https://doc.rust-lang.org/src/alloc/collections/btree/map.rs.html".to_string(),
                "https://doc.rust-lang.org/src/core/any.rs.html".to_string(),
                "https://doc.rust-lang.org/src/core/borrow.rs.html".to_string(),
                "https://doc.rust-lang.org/src/core/convert.rs.html".to_string(),
                "https://doc.rust-lang.org/std/any/struct.TypeId.html".to_string(),
                "https://doc.rust-lang.org/std/any/trait.Any.html".to_string(),
                "https://doc.rust-lang.org/std/borrow/trait.Borrow.html".to_string(),
                "https://doc.rust-lang.org/std/borrow/trait.BorrowMut.html".to_string(),
                "https://doc.rust-lang.org/std/cmp/trait.Ord.html".to_string(),
                "https://doc.rust-lang.org/std/collections/btree_map/index.html".to_string(),
                "https://doc.rust-lang.org/std/collections/btree_map/struct.BTreeMap.html"
                    .to_string(),
                "https://doc.rust-lang.org/std/collections/btree_map/struct.OccupiedEntry.html"
                    .to_string(),
                "https://doc.rust-lang.org/std/collections/btree_map/struct.VacantEntry.html"
                    .to_string(),
                "https://doc.rust-lang.org/std/collections/index.html".to_string(),
                "https://doc.rust-lang.org/std/convert/enum.Infallible.html".to_string(),
                "https://doc.rust-lang.org/std/convert/trait.From.html".to_string(),
                "https://doc.rust-lang.org/std/convert/trait.Into.html".to_string(),
                "https://doc.rust-lang.org/std/convert/trait.TryFrom.html".to_string(),
                "https://doc.rust-lang.org/std/convert/trait.TryInto.html".to_string(),
                "https://doc.rust-lang.org/std/default/trait.Default.html".to_string(),
                "https://doc.rust-lang.org/std/fmt/struct.Error.html".to_string(),
                "https://doc.rust-lang.org/std/fmt/struct.Formatter.html".to_string(),
                "https://doc.rust-lang.org/std/fmt/trait.Debug.html".to_string(),
                "https://doc.rust-lang.org/std/future/trait.Future.html".to_string(),
                "https://doc.rust-lang.org/std/index.html".to_string(),
                "https://doc.rust-lang.org/std/io/trait.Read.html".to_string(),
                "https://doc.rust-lang.org/std/io/trait.Write.html".to_string(),
                "https://doc.rust-lang.org/std/iter/trait.Iterator.html".to_string(),
                "https://doc.rust-lang.org/std/marker/trait.Send.html".to_string(),
                "https://doc.rust-lang.org/std/marker/trait.Sized.html".to_string(),
                "https://doc.rust-lang.org/std/marker/trait.Sync.html".to_string(),
                "https://doc.rust-lang.org/std/marker/trait.Unpin.html".to_string(),
                "https://doc.rust-lang.org/std/ops/trait.FnOnce.html".to_string(),
                "https://doc.rust-lang.org/std/primitive.reference.html".to_string(),
                "https://doc.rust-lang.org/std/primitive.unit.html".to_string(),
                "https://doc.rust-lang.org/std/result/enum.Result.html".to_string(),
            ],
        );
    }
}
