//! Functions to return JSON-formatted bodies for responding to client requests.

use crate::domain::Domain;
use json::object;
use std::collections::BTreeSet;

const MESSAGE: &str = "message";
const ERROR: &str = "error";

pub(crate) fn bad_request_body(error: &str) -> String {
    let body = object! {
        MESSAGE => "Invalid request",
        ERROR => error,
    };
    body.dump()
}

pub(crate) fn missing_domain_body(domain: &Domain) -> String {
    let body = object! {
        MESSAGE => format!(
            "Domain {} has not been requested to be crawled",
            domain
        ),
    };
    body.dump()
}

pub(crate) fn list_urls_body(domain: &Domain, opt_urls: &Option<BTreeSet<String>>) -> String {
    match opt_urls {
        Some(urls) => {
            let urls_vec = urls.iter().cloned().collect::<Vec<String>>();
            let body = object! {
                MESSAGE => format!("Unique URL list for {}", domain),
                "urls" => urls_vec,
            };
            body.dump()
        }
        None => is_being_crawled(domain),
    }
}

pub(crate) fn count_urls_body(domain: &Domain, opt_urls: &Option<BTreeSet<String>>) -> String {
    match opt_urls {
        Some(urls) => {
            let body = object! {
                MESSAGE => format!("Unique URL count for {}", domain),
                "count" => urls.len(),
            };
            body.dump()
        }
        None => is_being_crawled(domain),
    }
}

fn is_being_crawled(domain: &Domain) -> String {
    let body = object! {
        MESSAGE => format!("Domain {} is currently being crawled", domain),
    };
    body.dump()
}
