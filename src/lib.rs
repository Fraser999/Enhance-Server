//! A basic server implementing a web-crawler.

#![doc(test(attr(forbid(warnings))))]
#![warn(unused, missing_copy_implementations, missing_docs)]
#![deny(
    deprecated_in_future,
    future_incompatible,
    macro_use_extern_crate,
    rust_2018_idioms,
    nonstandard_style,
    single_use_lifetimes,
    trivial_casts,
    trivial_numeric_casts,
    unreachable_pub,
    unsafe_code,
    unstable_features,
    unused_import_braces,
    unused_lifetimes,
    unused_qualifications,
    unused_results,
    warnings,
    clippy::all,
    clippy::pedantic
)]
#![forbid(
    const_err,
    duplicate_macro_exports,
    exceeding_bitshifts,
    invalid_type_param_default,
    legacy_constructor_visibility,
    legacy_directory_ownership,
    macro_expanded_macro_exports_accessed_by_absolute_paths,
    missing_fragment_specifier,
    mutable_transmutes,
    no_mangle_const_items,
    order_dependent_trait_objects,
    overflowing_literals,
    parenthesized_params_in_types_and_modules,
    pub_use_of_private_extern_crate,
    safe_extern_statics,
    unknown_crate_types
)]
#![allow(unreachable_pub)]

mod crawl;
mod domain;
mod logging;
mod response_body;
mod server;

use futures::{future, Future};
use hyper::{client::HttpConnector, service, Body, Client, Server};
use hyper_tls::HttpsConnector;
use log::{error, info, warn};
use unwrap::unwrap;

pub(crate) type HttpsClient = Client<HttpsConnector<HttpConnector>>;

/// Construct a server and run it.  Blocks forever.
pub fn run() {
    logging::init();

    let address = unwrap!("0.0.0.0:7878".parse());

    hyper::rt::run(future::lazy(move || {
        let https = unwrap!(HttpsConnector::new(num_cpus::get() / 2));
        let client = Client::builder().build::<_, Body>(https);

        let new_service = move || {
            let client = client.clone();
            service::service_fn(move |request| server::handle_request(&request, &client))
        };

        let server = Server::bind(&address)
            .serve(new_service)
            .map_err(|e| error!("server error: {}", e));

        info!("'POST http://{}?<url>' to start crawling <url>", address);
        info!(
            "'GET http://{}/list?<url>' to get list of unique URLs indexed for domain at <url>",
            address
        );
        info!(
            "'GET http://{}/count?<url>' to get count of unique URLs indexed for domain at <url>",
            address
        );

        server
    }));
}
