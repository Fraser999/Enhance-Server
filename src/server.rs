//! The top-level handler for incoming client requests.

use crate::{
    crawl::{self, State},
    domain::Domain,
    response_body::*,
    HttpsClient,
};
use futures::{future, Future};
use http::uri::PathAndQuery;
use hyper::{Body, Method, Request, Response, StatusCode};
use lazy_static::lazy_static;
use log::debug;
use std::{
    collections::{BTreeSet, HashMap},
    sync::RwLock,
};
use unwrap::unwrap;

/// Map of domain to all unique paths for that domain.  When crawling starts for a given domain, an
/// entry will be added with the mapped value as `None`.  Once crawling is complete, the set of
/// unique URLs will be entered as `Some(set)`.
pub(crate) type Domains = RwLock<HashMap<Domain, Option<BTreeSet<String>>>>;

lazy_static! {
    pub(crate) static ref DOMAINS: Domains = RwLock::new(HashMap::default());
}

/// The main server function for handling new client requests.
///
/// The `client` passed in here is our own client for use in case the request triggers a new crawl.
pub(crate) fn handle_request(
    request: &Request<Body>,
    client: &HttpsClient,
) -> Box<dyn Future<Item = Response<Body>, Error = hyper::Error> + Send> {
    let bad_request = |error| {
        debug!("Returning BAD_REQUEST for {:?}", request);
        let body = Body::from(bad_request_body(error));
        Box::new(future::ok(unwrap!(Response::builder()
            .status(StatusCode::BAD_REQUEST)
            .body(body))))
    };

    let query = match request.uri().path_and_query().and_then(PathAndQuery::query) {
        Some(query) => query,
        None => return bad_request("Missing query"),
    };

    match (request.method(), request.uri().path()) {
        (&Method::POST, "/") => {
            let response = match crawl(query, client) {
                Ok(()) => unwrap!(Response::builder()
                    .status(StatusCode::ACCEPTED)
                    .body(Body::empty())),
                Err(error) => return bad_request(&error),
            };
            Box::new(future::ok(response))
        }
        (&Method::GET, "/list") => Box::new(future::ok(list(query))),
        (&Method::GET, "/count") => Box::new(future::ok(count(query))),
        _ => bad_request("Invalid request type or path"),
    }
}

/// Handles the client's POST request to crawl the given domain.  The client's response is sent
/// without waiting for the crawl to complete.
fn crawl(domain_str: &str, client: &HttpsClient) -> Result<(), String> {
    debug!("Crawling {}", domain_str);
    let state = State::new(domain_str, client.clone())?;
    let _ = hyper::rt::spawn(crawl::crawl(state));
    Ok(())
}

/// Handles the client's GET request for a list of unique URLs indexed for the given domain.
fn list(domain_str: &str) -> Response<Body> {
    debug!("Getting list for {}", domain_str);
    // TODO: Consider not returning 200 OK for non-existent or still-being-crawled domain?
    query_domains(domain_str, QueryType::List)
}

/// Handles the client's GET request for a count of unique URLs indexed for the given domain.
fn count(domain_str: &str) -> Response<Body> {
    debug!("Getting count for {}", domain_str);
    // TODO: Consider not returning 200 OK for non-existent or still-being-crawled domain?
    query_domains(domain_str, QueryType::Count)
}

#[derive(Copy, Clone, Eq, PartialEq)]
enum QueryType {
    List,
    Count,
}

fn query_domains(domain_str: &str, query_type: QueryType) -> Response<Body> {
    let domain = match Domain::new(domain_str) {
        Ok(domain) => domain,
        Err(error) => {
            return unwrap!(Response::builder()
                .status(StatusCode::BAD_REQUEST)
                .body(Body::from(bad_request_body(&error))));
        }
    };
    let domains = unwrap!(DOMAINS.read());
    let missing = || missing_domain_body(&domain);
    let found = |opt_urls: &Option<BTreeSet<String>>| {
        if query_type == QueryType::List {
            list_urls_body(&domain, opt_urls)
        } else {
            count_urls_body(&domain, opt_urls)
        }
    };
    let body = domains.get(&domain).map_or_else(missing, found);
    unwrap!(Response::builder()
        .status(StatusCode::OK)
        .body(Body::from(body)))
}
