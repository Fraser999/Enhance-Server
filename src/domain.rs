//! A helper struct representing a single domain, containing the domain and the scheme.

use std::{
    cmp::Ordering,
    fmt::{self, Debug, Display, Formatter},
    hash::{Hash, Hasher},
};
use url::Url;

/// We want to map results using the domain as the key.  However, we also need to retain the scheme
/// (e.g http/https/ftp) to be able to crawl and present results to the client.
///
/// This struct will hold the domain and scheme, but will only consider the domain value in the
/// context of equality, sorting or hashing.
#[derive(Clone)]
pub(crate) struct Domain {
    // The full serialised value, e.g. "https://example.com".
    scheme_and_domain: String,
    // Index of start of domain portion.
    domain_start: usize,
}

impl Domain {
    pub fn new(input: &str) -> Result<Self, String> {
        let error = || format!("Can't get domain from {}", input);
        let url = Url::parse(input).map_err(|_| error())?;
        let domain = url.domain().ok_or_else(error)?;
        Ok(Self {
            scheme_and_domain: format!("{}://{}", url.scheme(), domain),
            domain_start: url.scheme().len() + 3,
        })
    }

    pub fn scheme_and_domain(&self) -> &str {
        &self.scheme_and_domain
    }

    pub fn domain(&self) -> &str {
        &self.scheme_and_domain[self.domain_start..]
    }
}

impl PartialEq for Domain {
    fn eq(&self, other: &Self) -> bool {
        self.domain() == other.domain()
    }
}

impl Eq for Domain {}

impl Ord for Domain {
    fn cmp(&self, other: &Self) -> Ordering {
        self.domain().cmp(other.domain())
    }
}

impl PartialOrd for Domain {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl Hash for Domain {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.domain().hash(state);
    }
}

impl Debug for Domain {
    fn fmt(&self, formatter: &mut Formatter<'_>) -> fmt::Result {
        write!(formatter, "Domain({})", self.scheme_and_domain,)
    }
}

impl Display for Domain {
    fn fmt(&self, formatter: &mut Formatter<'_>) -> fmt::Result {
        write!(formatter, "{}", self.scheme_and_domain,)
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::logging;
    use std::collections::{BTreeSet, HashSet};
    use unwrap::{unwrap, unwrap_err};

    #[test]
    fn member_methods() {
        logging::init();
        unwrap_err!(Domain::new(""));
        unwrap_err!(Domain::new("tmp"));
        unwrap_err!(Domain::new("tmp/foo"));
        unwrap_err!(Domain::new("file:///tmp/foo"));
        unwrap_err!(Domain::new("blob:https://example.com/foo"));

        let domain = unwrap!(Domain::new("http://example.com/foo"));
        assert_eq!("http://example.com", domain.scheme_and_domain());
        assert_eq!("example.com", domain.domain());
    }

    #[test]
    fn trait_impls() {
        logging::init();
        // These all have domain as "example.com".
        let http_domain = unwrap!(Domain::new("http://example.com"));
        let https_domain = unwrap!(Domain::new("https://example.com/"));
        let ftp_domain = unwrap!(Domain::new("ftp://example.com/foo"));
        let abc_domain = unwrap!(Domain::new("abc://example.com/foo?opt"));

        // Equality.
        assert_eq!(http_domain, https_domain);
        assert_eq!(http_domain, ftp_domain);
        assert_eq!(http_domain, abc_domain);

        // Ordering (only really care that equivalence is correct).
        let set = [http_domain, https_domain, ftp_domain, abc_domain];
        assert_eq!(1, set.iter().cloned().collect::<BTreeSet<_>>().len());

        // Hashing (only really care that it matches the behaviour of equality).
        assert_eq!(1, set.iter().cloned().collect::<HashSet<_>>().len());
    }
}
