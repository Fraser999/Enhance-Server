# Enhance Server

A basic server implementing a web-crawler.

To run the server with logging enabled:
```
RUST_LOG=enhance_server=trace cargo run --release
```

* To request the server to start crawling for unique URLs on a given domain, send a POST to the
server with the required domain's address.  E.g.
```
curl -X POST 'http://127.0.0.1:7878?https://duckduckgo.com'
```

* To request the indexed list or count of unique URLs for a given domain, send a GET to the server
with the following format:
```
curl 'http://127.0.0.1:7878/list?https://duckduckgo.com'
curl 'http://127.0.0.1:7878/count?https://duckduckgo.com'
```

All responses are in JSON, and error messages provide information about why the request wasn't
satisfied.  For example, GETs will only return successful results once the crawl of that domain is
completed; no interim results are available.

## Notes/TODOs

* Improve test coverage.
* The results of a single crawl are held in memory.  These could be periodically dumped to disk if
exceeding a limit and replaced with a bloom filter to reduce memory consumption.
* The global results are held in memory.  This should probably be replaced by an on-disk database.
* Individual domains can be crawled again immediately after one has completed.  There should be a
mechanism to avoid overly frequent re-crawls of a single domain.
* Domains' robots.txt shouldn't be ignored.
* The behaviour could be updated to support clients connecting and receiving a stream of info from a
crawl which is still under way.
