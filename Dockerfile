
FROM rust:latest as rust-builder

COPY . .

RUN cargo build --release

EXPOSE 7878

ENV RUST_LOG=enhance_server=trace

CMD ["./target/release/enhance_server"]
